/* eslint-disable */
const withCss = require('@zeit/next-css')
const withLess = require('@zeit/next-less')
const lessToJS = require('less-vars-to-js')
const fs = require('fs')
const path = require('path')


// fix: prevents error when .css files are required by node
if (typeof require !== 'undefined') {
  require.extensions['.css'] = (file) => {}
}

// Where your antd-custom.less file lives
// const themeVariables = lessToJS(
//   fs.readFileSync(
//     path.resolve(__dirname, './static/asserts/antd-custom.less'),
//     'utf8'
//   )
// )
console.log(__dirname)
// if (typeof require !== 'undefined') {
//   require.extensions['.less'] = (file) => {}
// }
module.exports = withCss(withLess());
// module.exports = withCss({
//   cssModules: true,
//   lessLoaderOptions: {
//     javascriptEnabled: true,
//     importLoaders: 1,
//     modifyVars: themeVariables // make your antd custom effective
//   },
// })
