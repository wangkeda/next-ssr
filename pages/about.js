import { Button } from 'antd-mobile'
import React from 'react'
import Link from 'next/link'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Layout from '../components/Layout'
import { getSearchList } from '../store/actions'
import { http } from '../utils'

import './about.less'

class About extends React.Component {
  static async getInitialProps  (ctx) {
    const {req} = ctx
    const isServer = !!req
    const res = await http.get("/openapi/rest/v1/cn.meadin.open/mbi/mbi_type.get", null, isServer)
    const jsonData = await res.data
    return jsonData
    
  }
  render () {
    const { list } = this.props;
    return (
      <Layout title='Index'>
        <Link href='/index'>
          <Button>Go to About</Button>
        </Link>
        {list.map(function(item, indx){
　　　　　　　　return <h5 key={indx}>{item.typeName}</h5>
　　　　　　})}
      </Layout>
    )
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    getSearchList: bindActionCreators(getSearchList, dispatch)
  }
}
export default connect(mapDispatchToProps)(About);