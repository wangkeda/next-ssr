import React from 'react'
import {Provider} from 'react-redux'
import App, {Container} from 'next/app'
// import withRedux from 'next-redux-wrapper'

import withReduxStore from '../lib/with-redux-store'

export default withReduxStore(class MyApp extends App {
  static async getInitialProps ({Component, ctx}) {
    return {
      pageProps: (Component.getInitialProps ? await Component.getInitialProps(ctx) : {})
    }
  }

  render () {
    const {Component, pageProps, reduxStore} = this.props
    return <Container>
      <Provider store={reduxStore}>
        <Component {...pageProps} />
      </Provider>
    </Container>
  }
})