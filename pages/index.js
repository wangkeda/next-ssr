import { Button } from 'antd-mobile'
import React from 'react'
import Link from 'next/link'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Layout from '../components/Layout'
import TabBottom from '../components/TabBottom'
import TopMenu from '../components/TopMenu'
import Test from '../components/test'
import { getSearchList } from '../store/actions'
import { http } from '../utils'

import dynamic from 'next/dynamic'

const LineDemo = dynamic(import('../components/chart'), {
  ssr: false
})





class Counter extends React.Component {
  static async getInitialProps  (ctx) {
    // const {req} = ctx
    // const isServer = !!req
    // const res = await http.get("/openapi/rest/v1/cn.meadin.open/mbi/mbi_type.get", null, isServer)
    // const jsonData = await res.data
    // return jsonData
  }
  render () {
    const { list } = this.props;
    
    return (
      <Layout title='Index'>
        <TopMenu></TopMenu>
        <TabBottom selectedTab="首页" ></TabBottom>
      </Layout>
    )
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    getSearchList: bindActionCreators(getSearchList, dispatch)
  }
}
export default connect(mapDispatchToProps)(Counter);