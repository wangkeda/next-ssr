import { withRouter } from 'next/router'
import Head from 'next/head'


export default withRouter(({ router, children, title, description, keyword }) => (
  <div>
    <Head>
      <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1,viewport-fit=cover"></meta>
      <title>{title}</title>
      <meta content={description} name="description" />
      <meta content={keyword} name="keyword" />
      <meta name="author" />
    </Head>
    {children}
  </div>
))