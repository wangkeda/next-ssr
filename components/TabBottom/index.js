import {Flex} from 'antd-mobile';
import Router from 'next/router'
import Link from 'next/link'

import { common }  from '../../utils'
import './TabBottom.less';

const IconBox = (icon) => {
  return (
    <div className="icon-box">
      <img src={icon} />
    </div>
  )
}


const tabData = [
  {title: "首页", href: '/index', icon: `${common.img}/home-icon.png`, selectedIcon: `${common.img}/home-icon-a.png`  },
  {title: "关注", href: '/guanzhu', icon: `${common.img}/guzhu-icon.png`, selectedIcon: `${common.img}/guzhu-icon-a.png`  },
  {title: "MBI", href: '/hotelrankings', icon: `${common.img}/mbi.png`, selectedIcon: `${common.img}/mbi-a.png`  },
  {title: "MCI", href: '/', icon: `${common.img}/mci.png`, selectedIcon: `${common.img}/mci-a.png` },
];

export default ({selectedTab}) => (
  <nav className="tab-bottom-box">
    <Flex justify="around">
        {
          tabData.map((item, index) => {
            const { title, href, icon, selectedIcon } = item;
            return (
              <Link key={`bt-h${index}`} href={href} >
                <a className="bottom-href"  >
                  <img src={selectedTab === title? selectedIcon : icon} />
                  <p className={selectedTab === title ? 'active' : ''}>{title}</p>
                </a>
              </Link>
            )
          })
        }
    </Flex>
  </nav>
)