import {actionTypes} from "./action-types";

const exampleInitialState = {}

export const reducer = (state = exampleInitialState, action) => {
  switch (action.type) {
    case actionTypes.SEARCH_DATA:
      return Object.assign({}, state, { searchData: action.searchData })
    default: return state
  }
}