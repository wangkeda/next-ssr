import {actionTypes} from "./action-types";
import { http } from '../utils'

export const getSearchList = async (isServer) => {
  const res = await http.get("/openapi/rest/v1/cn.meadin.open/mbi/mbi_type.get", null, isServer)
  const jsonData = await res.data
  return jsonData

}